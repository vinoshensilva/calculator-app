module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        LeagueSpartan: ['League Spartan']
      }
    },
  },
  daisyui: {
    // themes: ["night","dracula","light"]
    themes: ["dracula"]
  },
  plugins: [require("daisyui")],
}