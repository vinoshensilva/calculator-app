// Import libraries
import { 
    createSlice, 
    PayloadAction 
} from '@reduxjs/toolkit';

// Import constants
import { 
    DIVIDE, 
    MINUS, 
    MULTIPLY, 
    PLUS,
    DOT,
} from '@/constants/operations';

// Type definitions
type TOperatorPayload = {
    operator: string;
}

type TOperandPayload = {
    operand: string;
}

type calculateState = {
    operand: string;
    operand2: string;
    operator: string;
}

const initialState = {
    operand: '0',
    operand2: '',
    operator: ''
};

function getCurrentOperandKey(state: calculateState){
    return (!state.operator ? 'operand' : 'operand2');
}
  
const calculatorSlice = createSlice({
    name: 'calculator',
    initialState,
    reducers: {
        setOperation: (state: calculateState,{payload}: PayloadAction<TOperatorPayload>) => {
            if(state.operand){
                state.operator = payload.operator;
            }
        },
        setOperand: (state: calculateState,{payload}: PayloadAction<TOperandPayload>) => {

            const {
                operand
            } = payload;

            
            function hasDotNotation(val: string){
                return val.includes(DOT);
            }
            
            const key = getCurrentOperandKey(state);
            
            // To prevent double dots being inserted into any of the operands
            if(hasDotNotation(state[key]) && operand === DOT){
                return;
            }

            state[key] = state[key] === '0' ? operand : state[key].concat(operand);
        },
        deleteSymbol: (state: calculateState)=>{
            const key = getCurrentOperandKey(state);

            state[key] = state[key].substring(0,state[key].length-1);
        },
        reset: ()=>{
            return initialState;
        },
        calculate: (state: calculateState)=>{

            let operand = state.operand ? parseFloat(state.operand): 0;
            let operand2 = state.operand2 ? parseFloat(state.operand2) : 0;

            const {
                operator
            } = state;

            // Value which will be assigned to operand after the calculation
            let newValue = 0;

            if(!operator){
                return state;
            }

            switch(operator){
                case PLUS:
                    newValue = operand + operand2; 
                break;
                case MINUS:
                    newValue = operand - operand2; 
                break;
                case MULTIPLY:
                    newValue = operand * operand2; 
                break;
                case DIVIDE:
                    if(!operand2){
                        throw new Error('Invalid dividend ' + operand2.toString());
                    }

                    newValue = operand / operand2;
                break;
            }

            return {
                ...initialState,
                operand: newValue.toString()
            }
        }
    },
});

export const calculatorReducer = calculatorSlice.reducer;

export const { 
    setOperation,
    setOperand,
    calculate,
    reset,
    deleteSymbol
} = calculatorSlice.actions;