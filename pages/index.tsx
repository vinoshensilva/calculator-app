// Import libraries
import { useDispatch, useSelector } from "react-redux";

// Import types
import type { NextPage } from 'next'
import { TStore } from '../store';

// Import slices
import { 
  setOperand,
  setOperation,
  calculate,
  deleteSymbol,
  reset
} from "../slices/calculator";

// Import components
import Button from '@/components/atom/button';

// Import constants
import { 
  DIVIDE, 
  DOT, 
  MINUS, 
  MULTIPLY, 
  PLUS 
} from "@/constants/operations";

const Home: NextPage = () => {
  const dispatch = useDispatch();
  
  const { 
    operand,
    operand2 
  } = useSelector((state: TStore) => state.calculatorReducer);

  function onClickOperator(val: string): void{
    dispatch(setOperation({operator: val}));
  }

  function onClickOperand(val: string): void{
    dispatch(setOperand({operand:val}));
  }

  function onClickCalculate(): void{
    dispatch(calculate());
  }

  function onClickDelete(): void{
    dispatch(deleteSymbol());
  }

  function onClickReset(): void{
    dispatch(reset());
  }

  function renderOperandButton(val : string){
    return <Button
      onClick={()=>{onClickOperand(val)}}
      key={val}
      className="btn text-lg btn-secondary md:w-20"
    >
      {val}
    </Button>
  }

  function renderOperatorButton(val : string){
    return <Button
      onClick={()=>{onClickOperator(val)}}
      key={val}
      className="btn text-lg btn-secondary"
    >
      {val}
    </Button>
  }

  return (
    <div className="flex font-LeagueSpartan items-center justify-center h-screen">
      <div className="grid grid-flow-row gap-2">
        <div className="bg-neutral text-2xl p-3 rounded-md text-lg text-right border-primary">
          {operand2 || operand || "0"}
        </div>
        <div className="grid grid-flow-row gap-2">
            <div className="grid gap-2 grid-cols-4 grid-flow-col">
              {renderOperandButton('7')}
              {renderOperandButton('8')}
              {renderOperandButton('9')}
    
              <Button
                onClick={onClickDelete}
                className="btn text-lg btn-primary"
              >
                DEL
              </Button>
            </div>

            <div className="grid gap-2 grid-cols-4 grid-flow-col">
              {renderOperandButton('4')}
              {renderOperandButton('5')}
              {renderOperandButton('6')}
              {renderOperatorButton(PLUS)}
            </div>

            <div className="grid gap-2 grid-cols-4 grid-flow-col">
              {renderOperandButton('1')}
              {renderOperandButton('2')}
              {renderOperandButton('3')}
              {renderOperatorButton(MINUS)}
            </div>

            <div className="grid gap-2 grid-cols-4 grid-flow-col">
              {renderOperandButton(DOT)}
              {renderOperandButton('0')}
              {renderOperatorButton(DIVIDE)}
              {renderOperatorButton(MULTIPLY)}
            </div>

            <div className="grid grid-cols-2 gap-2 grid-flow-col">
              <Button
                onClick={onClickReset}
                className="btn text-lg btn-primary w-full"
              >
                RESET
              </Button>
              <Button
                onClick={onClickCalculate}
                className="btn text-lg btn-accent w-full"
              >
                =
              </Button>
            </div>
        </div>
      </div>
    </div>
  );
}

export default Home
